package selecaoItens;

import java.util.ArrayList;
import java.util.List;

public class SelecaoItens {

	public static int LIMITE_PESO_MOCHILA = 8;
	private static List<Item> itensMochila = new ArrayList<>();

	public static void main(String[] args) {
		var itensPossiveis = inserirItensLista();

		System.out.println("Algoritmo Guloso");
		visualizarItensMochila(algoritmoGuloso(itensPossiveis));

		System.out.println("\n\nPrograma��o din�mica");
		visualizarItensMochila(programacaoDinamica(itensPossiveis));

	}

	private static List<Item> algoritmoGuloso(List<Item> itensPossiveis) {
		var somaItens = 0;

		for (Item item : itensPossiveis) {
			if ((somaItens + item.getPeso()) <= LIMITE_PESO_MOCHILA) {
				somaItens += item.getPeso();
				itensMochila.add(item);
			}
		}
		return itensMochila;
	}

	private static List<Item> programacaoDinamica(List<Item> itensPossiveis) {
		List<Item> itensMochila = new ArrayList<>();

		int[][] matriz = new int[itensPossiveis.size()][LIMITE_PESO_MOCHILA + 1];

		for (int linha = 0; linha < itensPossiveis.size(); linha++) {
			for (int coluna = 0; coluna <= LIMITE_PESO_MOCHILA; coluna++) {
				if (linha == 0 || coluna == 0) {
					matriz[linha][coluna] = 0;
				} else if (itensPossiveis.get(linha - 1).getPeso() <= coluna) {
					matriz[linha][coluna] = max(
							itensPossiveis.get(linha - 1).getValor()
									+ matriz[linha - 1][coluna - itensPossiveis.get(linha - 1).getPeso()],
							matriz[linha - 1][coluna]);
				}

				else {
					matriz[linha][coluna] = matriz[linha - 1][coluna];
				}
			}
		}

		imprimirMatriz(itensPossiveis, matriz);

		int i = itensPossiveis.size() - 1;
		int j = LIMITE_PESO_MOCHILA;
		while (i > 0 && j > 0) {

			if (matriz[i][j] != matriz[i - 1][j]) {
				itensMochila.add(itensPossiveis.get(i - 1));
				j = j - itensPossiveis.get(i - 1).getPeso();
			} else {
			}

			i--;
		}
		return itensMochila;
	}

	private static void imprimirMatriz(List<Item> itensPossiveis, int[][] matriz) {
		for (int i = 0; i < itensPossiveis.size(); i++) {
			for (int j = 0; j <= LIMITE_PESO_MOCHILA; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println("");
		}
	}

	static int max(int a, int b) {
		return (a > b ? a : b);
	}

	private static List<Item> inserirItensLista() {
		var itens = new ArrayList<Item>();

		itens.add(new Item("caderno", 3, 15));
		itens.add(new Item("camiseta", 4, 20));
		itens.add(new Item("notebook", 5, 18));
		itens.add(new Item("celular", 2, 25));
		itens.add(new Item("caneca", 1, 5));

		return itens;
	}

	private static void visualizarItensMochila(List<Item> itensMochila) {
		int somaPesoItens = itensMochila.stream().mapToInt(item -> item.getPeso()).sum();

		System.out.println("\nItens escolhidos: ");
		System.out.println("Peso limite/total: " + LIMITE_PESO_MOCHILA + "/" + somaPesoItens);
		itensMochila.forEach(item -> System.out.println(item.toString()));
	}
}
