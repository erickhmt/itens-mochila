package selecaoItens;

public class Item {

	private String nome;
	private Integer peso;
	private Integer valor;

	public Item(String nome, Integer peso, Integer valor) {
		super();
		this.nome = nome;
		this.peso = peso;
		this.valor = valor;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getPeso() {
		return peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "item: " + nome.toUpperCase() + " valor: " + valor + " peso: " + peso;
	}
}
